import java.util.Scanner;

public class CombatCalculator7
{
    public static void main(String[] args)
    {
        //Initialize monster and player variables
        
        /*Monster data variable*/
            
        //Initialize monster name variable and set it to"Goblin"
        String monster = "Goblin";
        //Initialize monster health variable and set it to 100
        int enemyHP = 100;
        //Initialize monster attack power variable and set it to 15
        int enemyAP = 15;
            
        /*Player variables*/
            
        //Initialize health variable and set it to 100
        int hp = 100;
        //Initialize player attack power variable and set it to 12
        int ap = 12;
        //Initialize player magic power varaible and set it to 0
        int mp = 0;
        
        /*Loop Control*/
        //Declare loop control variable and intialize it to true
        boolean battle = true;
        
        //While the loop control variable is true
        while (battle == true)
        {
            /*Report Combat Stats*/
     
            //Print Monsters Names
            System.out.printf("\nYou are fighting a %s..\n", monster);
            //Print Monsters Health
            System.out.printf(monster + "'s HP: " + enemyHP);
            //Print Player's Health
            System.out.printf("\nYour HP: " + hp);
            //Print the Player's magic points
            System.out.printf("\nYour MP: " + mp);
            System.out.printf("\n");
            
            /*Combat Menu Prompt*/
            System.out.printf("\nCombat Options:\n");
        
            //Print Sword Attack
            System.out.printf("  1.) Sword Attack\n");
            //Print Spell
            System.out.printf("  2.) Cast Spell\n");
            //Charge Mana
            System.out.printf("  3.) Charge Mana\n");
            //Run Away
            System.out.printf("  4.) Run Away\n");
            
            //Prompt for action
            System.out.printf("\nWhat are you going to do?\n");
                
            //Declare variable for user input as number and aquire new scanner object
            Scanner input = new Scanner(System.in);
            int choice = input.nextInt();
            
                    //Calculate damage & update monster health using subtraction
                    //Calculation
                    if (choice==1)
                    {
                        enemyHP -= ap;
                        System.out.printf("\nYou strike the %s with your sword for %s damage!\n", monster, ap);
                    }
                    
                    else if (choice==2)
                    {
                        if (mp>=3)
                        {
                            enemyHP = enemyHP/2;
                            System.out.printf("\nYou Cast lightning on the %s!\n", monster);
                        }
                        else
                        {
                            System.out.printf("\nYou don't have enough MP!\n");
                        }
                    }
                    
                    else if (choice==3)
                    {
                        mp = mp + 1;
                        System.out.printf("\nYou focus energy!\n MP is now %s!\n", mp);
                        System.out.printf("\n");
                    }
                    //Else if option 4 was chosen
                    else if (choice==4)
                    {
                        System.out.printf("\nYou run away from %s!\n", monster);
                        System.out.printf("\n");
                        battle = false;
                    }
                    //Else the player chose incorrectly
                    else
                    {
                        System.out.printf("\nI don't understand that command.\n");
                        System.out.printf("\n");
                    }
        //If monster's health is 0 or below
        if (enemyHP<=0)
        {
        //stop combat loop
        battle = false;
        //Print victory message
        System.out.printf("\nYou have defeated %s!\n", monster);
        }
        }
    }
}
  