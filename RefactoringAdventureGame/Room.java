public class Room
{
    
    public String description;
    Room north;
    Room east;
    Room west;
    Room south;

    //CONSTRUCTOR
    public Room (String description)
    {
        this.description = description;
    }
   
    public void setNorth(Room n)
    {
        this.north = n;
    }
    public void setEast(Room e)
    {
        this.east = e;
    }
    public void setWest(Room w)
    {
        this.west = w;
    }
    public void setSouth(Room s)
    {
        this.south = s;
    }
    
    public Room getNorth(Room n)
    {
        return north;
    }
    public Room getEast(Room e)
    {
        return east;
    }
    public Room getWest(Room w)
    {
        return west;
    }
    public Room getSouth(Room s)
    {
        return south;
    }

    public String GetDescription() {
        return description;
    }
}