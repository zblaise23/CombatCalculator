import java.util.Scanner;
import java.util.Random;

public class ExpandedCombatSystem2
{
    public static void main(String[] args)
    {
        
        //Initialize monster and player variables
        
        /*Monster data variable*/
            
        //Create 3 monsters
        Random rand = new Random();
        String[] enemies = { "Goblin", "Ork", "Troll" };
        //Initialize monster health variable and set it to 100
        int enemyHP = 100;
        //Initialize monster attack power variable and set it to 15
        int enemyAP = 15;
            
        /*Player variables*/
            
        //Initialize health variable and set it to 100
        int hp = 0;
        //Initialize player attack power variable and set it to 12
        int ap = 0;
        //Initialize player magic power varaible and set it to 0
        int mp = 0;
        //experience point for char creation
        int statpt = 20;
        
        /*Character Creation*/
        {
            boolean charcreate = true; 
            while (charcreate == true)
            {
                System.out.printf("\nHealth: %s, Attack: %s, Magic: %s\n", hp, ap, mp);
                System.out.printf(" 1.) +10 Health\n");
                System.out.printf(" 2.) +1 Attack\n");
                System.out.printf(" 3.) +3 Magic\n");
                System.out.printf("You have %s points to spend!\n", statpt);
                
                Scanner input = new Scanner(System.in);
                int choice1 = input.nextInt();
                if (choice1 == 1)
                {
                    statpt = statpt - 1;
                    hp = hp + 10;
                }
                else if (choice1 == 2)
                {
                    statpt = statpt - 1;
                    ap = ap + 1;
                }
                else if (choice1 == 3)
                {
                    statpt = statpt - 1;
                    mp = mp + 3;
                }
                else
                {
                    System.out.printf("\nInvalid command!\n");
                }
                if (statpt <= 0)
                {
                    break;
                }
                
            }
            
        }
        String monster = enemies[rand.nextInt(enemies.length)];
        /*Loop Control*/
        //Declare loop control variable and intialize it to true, //While the loop control variable is true
        boolean battle = true;
        while (battle == true)
        {
            /*Combat system*/
            
            
            //Generate Monster
            System.out.printf("\nYou encounter a %s..\n", monster);
            System.out.printf(monster + "'s HP: " + enemyHP);
            System.out.printf("\nYour HP: " + hp);
            System.out.printf("\nYour MP: " + mp);
            System.out.printf("\n");
            
            /*Combat Menu Prompt*/
            System.out.printf("\nCombat Options:\n");
        
            //Print Sword Attack
            System.out.printf("  1.) Sword Attack\n");
            //Print Spell
            System.out.printf("  2.) Cast Spell\n");
            //Charge Mana
            System.out.printf("  3.) Charge Mana\n");
            //Run Away
            System.out.printf("  4.) Run Away\n");
            
            //Prompt for action
            System.out.printf("\nWhat are you going to do?\n");
                
            //Declare variable for user input as number and aquire new scanner object
            Scanner input = new Scanner(System.in);
            int choice = input.nextInt();
            
                    //Calculate damage & update monster health using subtraction
                    //Calculation
                    if (choice==1)
                    {
                        int damage = rand.nextInt(ap);
                        enemyHP -= damage;
                        System.out.printf("\nYou strike the %s with your sword for %s damage!\n", monster, ap);
                    }
                    
                    else if (choice==2)
                    {
                        if (mp>=3)
                        {
                            enemyHP = enemyHP/2;
                            System.out.printf("\nYou Cast lightning on the %s!\n", monster);
                        }
                        else
                        {
                            System.out.printf("\nYou don't have enough MP!\n");
                        }
                    }
                    
                    else if (choice==3)
                    {
                        mp = mp + 1;
                        System.out.printf("\nYou focus energy!\n MP is now %s!\n", mp);
                        System.out.printf("\n");
                    }
                    //Else if option 4 was chosen
                    else if (choice==4)
                    {
                        System.out.printf("\nYou run away from %s!\n", monster);
                        System.out.printf("\n");
                        battle = false;
                    }
                    //Else the player chose incorrectly
                    else
                    {
                        System.out.printf("\nI don't understand that command.\n");
                        System.out.printf("\n");
                    }
        //If monster's health is 0 or below
        if (enemyHP<=0)
        {
        //stop combat loop
        battle = false;
        //Print victory message
        System.out.printf("\nYou have defeated %s!\n", monster);
        }
        }
    }
}
  