import java.util.Scanner;
import java.util.Random;

public class ExpandedCombatSystem3
{
    static Random rand = new Random();
    static String[] enemies = { "Goblin", "Demon", "Troll", "Skeleton", "Oogle" };
    static int enemyHP = 100;
    static int enemyAP = 15;
                
    static int level = 1;
    static int hp = 0;
    static int ap = 0;
    static int mp = 0;
    static int statpt = 20;
    
    //METHODS!!!
    public static void main(String[] args){
        charCreate();
        battleMethod();
    }
    public static void charCreate()
    {
        boolean charcreate = true;
        while (charcreate == true)
        {
            System.out.printf("Health: %s, Attack: %s, Magic: %s\n", hp, ap, mp);
            System.out.println(" 1.) +10 Health");
            System.out.println(" 2.) +1 Attack");
            System.out.println(" 3.) +3 Magic");
            System.out.printf("\nYou have %s points to spend!\n", statpt);
            
            Scanner input = new Scanner(System.in);
            int choice1 = input.nextInt();
            if (choice1==1){
                statpt = statpt - 1;
                hp = hp + 10;
            }
            else if (choice1==2){
                statpt = statpt - 1;
                ap = ap + 1;
            }
            else if (choice1==3){
                statpt = statpt - 1;
                mp = mp + 3;
            }
            else{
                System.out.println("Invalid command!");
            }
            if (statpt<=0){
                System.out.println("All Stat points depleted!");
                break;
            }
        }
    }
    public static void battleMethod(){
    String monster = enemies[rand.nextInt(enemies.length)];
    System.out.printf("\nYou encounter a %s..\n", monster);
    int monsterhealth= rand.nextInt(enemyHP);
    boolean battle = true;
    while (battle == true){
        System.out.printf("\n%s's HP: %s", monster, monsterhealth);
        System.out.printf("\nYour HP: " + hp);
        System.out.printf("\nYour MP: " + mp);
        System.out.printf("\n");
        
        /*Combat Menu Prompt*/
        System.out.println("Combat Options:");
        System.out.println("  1.) Sword Attack");
        System.out.println("  2.) Cast Spell");
        System.out.println("  3.) Charge Mana");
        System.out.println("  4.) Run Away");
        
        //Prompt for action
        System.out.println("What are you going to do?");
            
        //Declare variable for user input as number and aquire new scanner object
        Scanner input = new Scanner(System.in);
        int choice = input.nextInt();
            if (choice==1){
                int damdealt = rand.nextInt(ap);
                int damtak = rand.nextInt(enemyAP);
                monsterhealth -= damdealt;
                hp -= damtak;
                System.out.printf("You strike the %s with your sword for %s damage!\n", monster,  damdealt);
                System.out.printf("The " + monster + " strikes you for " + damtak + " damage!");
            }
            else if (choice==2){
                int damdealt= mp/2;
                int damtak = rand.nextInt(enemyAP);
                if (mp>=0){
                    monsterhealth -= damdealt;
                    hp -= damtak;
                    mp -= 1;
                    System.out.printf("\nYou Cast lightning on the %s, striking the creature for %s damage!", monster, damdealt);
                    System.out.printf("\nThe " + monster + " strikes you for " + damtak + " damage!\n");
                }
                if (mp<=0){
                    System.out.println("You don't have enough MP!");
                    mp = 0;
                }
            }
            else if (choice==3){
                mp = mp + 1;
                System.out.printf("You focus energy!\n MP is now %s!\n", mp);
                System.out.printf("\n");
            }
            //Else if option 4 was chosen
            else if (choice==4){
                System.out.printf("You run away from %s!", monster);
                System.out.printf("\n");
                battle = false;
            }
            else{
                System.out.println("I don't understand that command.");
                System.out.printf("\n");
            }
                
        if (monsterhealth<=0){
            System.out.printf("\nYou have defeated the %s!\n", monster);
            System.out.printf("You earned +1 experience!\n");
            System.out.printf("\n");
            statpt += 1;
            battle = false;
                }
                if (hp<=0){
                    battle = false;
                    System.out.println("Surprise, you're dead!");
                    System.out.println("Game Over!");
                }
    }
    }
}

