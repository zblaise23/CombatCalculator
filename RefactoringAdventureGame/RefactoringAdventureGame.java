import java.util.Scanner;

public class RefactoringAdventureGame
{
    public static void main(String[] args)
    {
        boolean game = true;
        Dungeon dungeon = new Dungeon();
        Room currentRoom = dungeon.getRoom();
        
        while (game == true)
        {
            System.out.println(currentRoom.description.toString());
            //System.out.println(currentRoom.exits.toString());
            System.out.println("Which direction would you like to go?");
            Scanner input = new Scanner(System.in);
            String choice = input.next();
            if (choice.equals("north")){
                if (currentRoom.north != null){
                currentRoom = currentRoom.north;
                }
                else{
                    System.out.println("That is a wall!");
                    System.out.println("Choose a valid direction...");
                }
            }
            else if (choice.equals("south")){
                if (currentRoom.south != null){
                currentRoom = currentRoom.south;
                }
                else{
                    System.out.println("That is a wall!");
                    System.out.println("Choose a valid direction...");
                }
            }
            else if (choice.equals("east")){
                if (currentRoom.east != null){
                currentRoom = currentRoom.east;
                }
                else{
                    System.out.println("That is a wall!");
                    System.out.println("Choose a valid direction...");
                }
            }
            else if (choice.equals("west")){
                if (currentRoom.west != null){
                currentRoom = currentRoom.west;
                }
                else{
                    System.out.println("That is a wall!");
                    System.out.println("Choose a valid direction...");
                }
            }
            else if (choice.equals("exit") || choice.equals("quit")){
                game = false;
            }
            else{
                System.out.println("Invalid command.");
            }
        }
        System.out.println("Thanks for playing!");
    }
}
