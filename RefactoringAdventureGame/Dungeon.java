import java.util.Scanner;

public class Dungeon
{
    
    public Room Processing;
    public Room Recroom;
    public Room Lobby;
    public Room Jailcell;
    public Room Gaschamber;
    public Room Incinerator;
    
    
    private Room currentRoom;
    private boolean game;
    
    public Dungeon() {
        InitializeRooms();
        SetNextRooms();
    }
    
    public void InitializeRooms() {
        Processing = new Room("You are in the processing center.");
        Recroom = new Room("This is the rec room.");
        Lobby = new Room("You've entered the lobby");
        Jailcell = new Room("You've entered a bare jailcell..");
        Gaschamber = new Room("Ewww a gas chamber. It still reeks!");
        Incinerator = new Room("the garbage incinerator. You get some strange vibes from this room...");
    }
    //Level Nodes
    public void SetNextRooms() {
        // Processing
        Processing.setNorth(null);
        Processing.setEast(Recroom);
        Processing.setWest(null);
        Processing.setSouth(Jailcell);
        //Recroom
        Recroom.setNorth(null);
        Recroom.setEast(Lobby);
        Recroom.setWest(Processing);
        Recroom.setSouth(Gaschamber);
        //Lobby
        Lobby.setNorth(null);
        Lobby.setEast(null);
        Lobby.setWest(Recroom);
        Lobby.setSouth(Incinerator);
        //Jailcell
        Jailcell.setNorth(Processing);
        Jailcell.setEast(Gaschamber);
        Jailcell.setWest(null);
        Jailcell.setSouth(null);
        //Gaschamber
        Gaschamber.setNorth(Recroom);
        Gaschamber.setEast(Incinerator);
        Gaschamber.setWest(Jailcell);
        Gaschamber.setSouth(null);
        //Incinerator
        Incinerator.setNorth(Lobby);
        Incinerator.setEast(null);
        Incinerator.setWest(Gaschamber);
        Incinerator.setSouth(null);
    }
    public Room getRoom(){
        return Processing;
    }
}