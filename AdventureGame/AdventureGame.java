import java.util.Scanner;

public class AdventureGame
{
    public static void main(String[] args)
    {
        boolean game = true;
        //Room decriptions
        String[] description={"You are in a jail cell.", "You are in a torture chamber.", 
                              "You are in a watchpost with a large 1 sided mirror facing the hallway.", 
                              "You are in a mess hall.", "You enter a brightly lit corriidor.", 
                              "You enter the incinerator."};
        //Room exits
        int[][] roomexits={
            {5,1,-1,-1}, //jail cell (room 1)
            {4,2,0,-1},  //torture chamber (room 2)
            {3,-1,1,-1}, //watchpost (room 3)
            {-1,-1,4,2}, //messhall (room 4)
            {-1,3,5,1},  //bright hallway (room 5)
            {-1,4,-1,0}};//incinerator 
            //Room variables
        int currentRoom = 0;
        int nextRoom = 0;
            //Direction variables
        int NORTH = 0; 
        int EAST = 1; 
        int WEST = 2; 
        int SOUTH = 3;
           
           /*game variable*/
        while (game == true)
        {
            Scanner input = new Scanner(System.in);
            String roomdescription = description[currentRoom];
            System.out.println(roomdescription);

            //Prompt User for Direction//
            
            System.out.println("Which direction are you going?");
            String choice = input.next();
                if(choice.equalsIgnoreCase("n"))
                {
                    nextRoom = roomexits[currentRoom][NORTH];
                    if (nextRoom != -1)
                    {
                        currentRoom = nextRoom;
                        System.out.println("You go north.");
                    }
                    else if (nextRoom == -1)
                    {
                        System.out.println("Can't go that way, thats a wall!");
                    }
                }
                else if(choice.equalsIgnoreCase("e")){
                    nextRoom = roomexits[currentRoom][EAST];
                    if (nextRoom != -1)
                    {
                        currentRoom = nextRoom;
                        System.out.println("You go east.");
                    }
                    else if (nextRoom == -1)
                    {
                        System.out.println("Can't go that way, thats a wall!");
                    }
                }
                else if(choice.equalsIgnoreCase("w")){
                    nextRoom = roomexits[currentRoom][WEST];
                    if (nextRoom != -1)
                    {
                        currentRoom = nextRoom;
                        System.out.println("You go west.");
                    }
                    else if (nextRoom == -1)
                    {
                        System.out.println("Can't go that way, thats a wall!");
                    }
                }
                else if(choice.equalsIgnoreCase("s")){
                    nextRoom = roomexits[currentRoom][SOUTH];
                    if (nextRoom != -1)
                    {
                        currentRoom = nextRoom;
                        System.out.println("You go south.");
                    }
                    else if (nextRoom == -1)
                    {
                        System.out.println("Can't go that way, thats a wall!");
                    }
                }
                else if(choice.equals("q"))
                {
                    System.out.println("You're tired of hanging out here, you leave.");
                    game = false;
                }
                else {System.out.println("That is not a valid option!");
                }
        }
    }
}
                